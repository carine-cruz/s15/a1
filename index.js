console.log("Hello World");


let num1 = Number(prompt('Enter a number:'));
let num2 = Number(prompt('Enter another number:'));
let numTotal = num1+num2;
let newTotal;

// step 3 -- compute total based on initial total
if(numTotal<10){
	newTotal = num1+num2;

/*	if (numTotal>=10){
		alert(`The sum is ${newTotal}`);
	}
	else{
		console.warn(`The sum is ${newTotal}`);
	}*/

}
else if(numTotal>=10 && numTotal<=20){
	newTotal = num1-num2;

/*	if (numTotal>=10){
		alert(`The difference is ${newTotal}`);
	}
	else{
		console.warn(`The difference is ${newTotal}`);
	}*/
}
else if(numTotal>=21 && numTotal<=30){
	newTotal = num1*num2;

/*	if (numTotal>=10){
		alert(`The product is ${newTotal}`);
	}
	else{
		console.warn(`The product is ${newTotal}`);
	}*/
}
else if(numTotal>30){
	newTotal = num1/num2;

/*	if (numTotal>=10){
		alert(`The quotient is ${newTotal}`);
	}
	else{
		console.warn(`The quotient is ${newTotal}`);
	}*/
}

// console.log(`num1: ${num1}`);
// console.log(`num2: ${num2}`);
// console.log(`numTotal: ${numTotal}`);
// console.log(`newTotal: ${newTotal}`);

// step 4 -- alert or console
if (newTotal>=10){
	alert(`The total is ${newTotal}`);
}
else{
	console.warn(`The total is ${newTotal}`);
}


// step 5 -- prompt for name and age
let name = prompt(`Enter name:`);
let age = parseInt(prompt(`Enter age:`));


if(name == "" || age == "" || name == null || age == null){
	alert(`Are you a time traveller?`);
}

if(name !== "" && age !== "" && name!== null && age !== null){
	alert(`Your name is ${name} \n Your age is ${age}`);
}

//step 6 - create function to check age legality
function isLegalAge(Age){
	if(Age>=18){
		alert(`You are of legal age.`);
	}
	else {
		alert(`You are not allowed here.`);
	}
} //end function isLegalAge

isLegalAge(age);

//step 7 - switch statement
switch(age){
	case 18: alert(`You are now allowed to party.`);
			 break;
	case 21: alert(`You are now part of the adult society.`);
			 break;
	case 65: alert(`We thank you for your contribution to society`);
			 break;
	default: alert(`Are you sure you're not an alien?`);
}


